--  ____        _ _    _
-- / ___| _ __ (_) | _(_)    Besnik Rrustemi (Sniki)
-- \___ \| '_ \| | |/ / |    https://gitlab.com/sniki/
--  ___) | | | | |   <| |
-- |____/|_| |_|_|_|\_\_|
--
-- This is my XMonad Window Manager config file with the stuff that i use.

--- Imports

-- Base
import XMonad

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

-- Utilities
import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.SpawnOnce
import XMonad.Util.Run
import XMonad.Util.Ungrab

-- Layouts
import XMonad.Layout.Magnifier
import XMonad.Layout.Spacing
import XMonad.Layout.ThreeColumns

main :: IO ()
main = xmonad
     . ewmhFullscreen
     . ewmh
     . withEasySB (statusBarProp "xmobar /home/besnik/.config/xmobar/xmobarrc" (pure myXmobarPP)) defToggleStrutsKey
     $ myConfig

myConfig = def
    { modMask     = mod4Mask                      -- Rebind Mod to the Super key
    , layoutHook  = spacingWithEdge 10 $ myLayout -- Use custom layouts
    , manageHook  = myManageHook                  -- Match on certain windows
    , startupHook = myStartupHook                 -- Startup Applications
    , terminal    = myTerminal                    -- Terminal of choice
    , borderWidth = myBorderWidth                 -- Set border width
    }
 `additionalKeysP`
    [ ("M-S-z",                   spawn "xscreensaver-command -lock")
    , ("M-C-s", unGrab *>         spawn "scrot -s"                  )
    , ("M-f"  ,                   spawn "librewolf"                 )
    , ("<XF86AudioMute>",         spawn "amixer sset Master toggle" )
    , ("<XF86AudioLowerVolume>",  spawn "amixer sset Master 5%-"    )
    , ("<XF86AudioRaiseVolume>",  spawn "amixer sset Master 5%+"    )
    , ("<XF86AudioMicMute>",      spawn "amixer sset Capture toggle")
    , ("<XF86MonBrightnessDown>", spawn "brightnessctl set 5%-"     )
    , ("<XF86MonBrightnessUp>",   spawn "brightnessctl set +5%"     )
    , ("<XF86Display>",           spawn ""                          )
    , ("<XF86WLAN>",              spawn ""                          )
    , ("<XF86Tools>",             spawn "alacritty"                 )
    , ("<XF86Search>",            spawn "dmenu_run -h 30 -nb '#282c34' -nf '#dfdfdf' -sb '#c678dd' -sf '#dfdfdf' -p Run:")
    , ("<XF86LaunchA>",           spawn "librewolf"                 )
    , ("<XF86Explorer>",          spawn "pcmanfm"                   )
    , ("<Print>",                 spawn ""                          )
    ]

myManageHook :: ManageHook
myManageHook = composeAll
    [ className =? "Gimp" --> doFloat
    , isDialog            --> doFloat
    ]

myLayout = tiled ||| Mirror tiled ||| Full ||| threeCol
  where
    threeCol = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    tiled    = Tall nmaster delta ratio
    nmaster  = 1      -- Default number of windows in the master pane
    ratio    = 1/2    -- Default proportion of screen occupied by master pane
    delta    = 3/100  -- Percent of screen to increment by when resizing panes

myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = magenta " • "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
    , ppHidden          = white . wrap " " ""
    , ppHiddenNoWindows = lowWhite . wrap " " ""
    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    "[") (white    "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow

    -- | Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    blue, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#ff79c6" ""
    blue     = xmobarColor "#bd93f9" ""
    white    = xmobarColor "#f8f8f2" ""
    yellow   = xmobarColor "#f1fa8c" ""
    red      = xmobarColor "#ff5555" ""
    lowWhite = xmobarColor "#bbbbbb" ""

myStartupHook = do
    spawnOnce "nitrogen --restore &"
    spawnOnce "picom &"
    spawnOnce "lxsession &"

myTerminal    = "alacritty"

myBorderWidth = 2
