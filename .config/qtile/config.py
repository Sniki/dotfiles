#  ____        _ _    _
# / ___| _ __ (_) | _(_)    Besnik Rrustemi (Sniki)
# \___ \| '_ \| | |/ / |    https://gitlab.com/sniki/
#  ___) | | | | |   <| |
# |____/|_| |_|_|_|\_\_|
#
# This is my Qtile Window Manager config file with the stuff that i use.

# Imports
import os, subprocess
from colors import color_scheme
from keybindings import keys
from libqtile.lazy import lazy
from libqtile import bar, hook, layout, widget
from libqtile.config import Click, Drag, DropDown, Group, Key, KeyChord, Match, ScratchPad, Screen
from libqtile.dgroups import simple_key_binder
#from qtile_extras import widget
#from qtile_extras.widget import StatusNotifier
#from qtile_extras.widget.decorations import BorderDecoration, PowerLineDecoration, RectDecoration

# Defaults
mod = "mod4"

# Colorschemes
colors = color_scheme["tokyonight-moon"]

# Workspaces
groups = [Group("", layout='columns'),
          Group("", layout='columns'),
          #Group("", layout='monadtall'),
          Group("", layout='columns'),
          #Group("", layout='monadtall'),
          #Group("", layout='monadtall'),
          Group("", layout='monadtall'),
          #Group("", layout='monadtall'),
          #Group("", layout='floating'),
          ScratchPad("scratchpad", [
              DropDown("term", "alacritty", width=0.5, height=0.6, x=0.25, y=0.1),
              DropDown("vim", "alacritty -e vim", width=0.5, height=0.6, x=0.25, y=0.1),
              DropDown("pcmanfm", "pcmanfm", width=0.5, height=0.6, x=0.25, y=0.1),
          ])]

# Layout Theme
layout_theme = {"border_width": 2,
                "margin": 10,
                "border_focus":  colors[11],
                "border_normal": colors[10],
               }

layouts = [
    layout.Columns(**layout_theme),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    layout.Floating(**layout_theme),
]

widget_defaults = dict(
    font="sans bold",
    fontsize=12,
    padding=5,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
                    scale=0.6,
                    background=colors[10]
                ),
                widget.Spacer(
                    length=5,
                    background=colors[0],
                ),
                widget.GroupBox(
                    active=colors[2],
                    inactive=colors[1],
                    highlight_method="block",
                    this_current_screen_border=colors[10],
                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Wttr(
                    format="%c Lipjan %t %C",
                    location={'Lipjan': 'Lipjan'},
                    mouse_callbacks={"Button1": lazy.spawn("alacritty --hold -e curl wttr.in/lipjan")},
                    units="m",
                    update_interval=300,
                    foreground=colors[5],
                ),
                widget.GenPollText(
                    func=lambda: subprocess.check_output(os.path.expanduser("~/.local/bin/kernel")).decode("utf-8"),
                    update_interval=600,
                    foreground=colors[3],
                ),
                widget.Memory(
                    format="{MemUsed: .1f}{mm}/{MemTotal: .1f}{mm}",
                    mouse_callbacks={"Button1": lazy.spawn("alacritty -e htop")},
                    measure_mem="G",
                    foreground=colors[6],
                ),
                widget.DF(
                    format=" {uf} {m}B",
                    visible_on_warn=False,
                    update_interval=60,
                    foreground=colors[8],
                ),
                widget.Backlight(
                    backlight_name="intel_backlight",
                    brightness_file="brightness",
                    max_brightness_file="max_brightness",
                    format="{percent:5.0%}",
                    step=5,
                    foreground=colors[4],
                ),
                widget.Volume(
                    fmt="  {}",
                    foreground=colors[9],
                ),
                widget.Battery(
                    format="{char} {percent:2.0%} ({hour:d}:{min:02d})",
                    charge_char="",
                    discharge_char=" ",
                    full_char="",
                    empty_char="",
                    unknown_char="",
                    update_interval=60,
                    show_short_text=False,
                    foreground=colors[7],
                ),
                widget.Clock(
                    format="  %A, %B %d (%H:%M)",
                    mouse_callbacks={"Button1": lazy.spawn("alacritty --hold -e cal")},
                    update_interval=60,
                    foreground=colors[2],
                ),
                # widget.StatusNotifier(),
                widget.Systray(
                    padding=0,
                ),
                widget.Spacer(
                    length=5,
                ),
            ],
            size=30,
            background=colors[0],
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
        # Choose the wallpaper and it's mode:
        # wallpaper="~/Pictures/Wallpapers/0007.jpg",
        # wallpaper_mode="stretch",
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = simple_key_binder("mod4")
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],  **layout_theme
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# Autostart
@hook.subscribe.startup_once
def start_once():
        home = os.path.expanduser('~')
        subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
