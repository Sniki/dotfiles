# Color Schemes
color_scheme = {
"tokyonight-night": 	[["#1a1b26"],   #colors[0] - Background (Black)
                         ["#c0caf5"],   #colors[1] - Foreground (White)
                         ["#7dcfff"],   #colors[2] - Cyan
                         ["#7aa2f7"],   #colors[3] - Blue 
                         ["#9d7cd8"],   #colors[4] - Purple
                         ["#bb9af7"],   #colors[5] - Pink
                         ["#f7768e"],   #colors[6] - Red
                         ["#ff9e64"],   #colors[7] - Orange
                         ["#e0af68"],   #colors[8] - Yellow
                         ["#9ece6a"],   #colors[9] - Green
                         ["#292e42"],   #colors[10] - Separator/Border Normal
                         ["#7dcfff"]],  #colors[11] - Border Focus
                         
"tokyonight-storm": 	[["#24283b"],   #colors[0] - Background (Black)
                         ["#c0caf5"],   #colors[1] - Foreground (White)
                         ["#7dcfff"],   #colors[2] - Cyan
                         ["#7aa2f7"],   #colors[3] - Blue 
                         ["#9d7cd8"],   #colors[4] - Purple
                         ["#bb9af7"],   #colors[5] - Pink
                         ["#f7768e"],   #colors[6] - Red
                         ["#ff9e64"],   #colors[7] - Orange
                         ["#e0af68"],   #colors[8] - Yellow
                         ["#9ece6a"],   #colors[9] - Green
                         ["#292e42"],   #colors[10] - Separator/Border Normal
                         ["#7dcfff"]],  #colors[11] - Border Focus

"tokyonight-moon":	 	[["#222436"],   #colors[0] - Background (Black)
                         ["#c8d3f5"],   #colors[1] - Foreground (White)
                         ["#86e1fc"],   #colors[2] - Cyan
                         ["#82aaff"],   #colors[3] - Blue 
                         ["#fca7ea"],   #colors[4] - Purple
                         ["#c099ff"],   #colors[5] - Pink
                         ["#ff757f"],   #colors[6] - Red
                         ["#ff966c"],   #colors[7] - Orange
                         ["#ffc777"],   #colors[8] - Yellow
                         ["#9ece6a"],   #colors[9] - Green
                         ["#2f334d"],   #colors[10] - Separator/Border Normal
                         ["#c099ff"]],  #colors[11] - Border Focus

"tokyonight-day":	 	[["#e1e2e7"],   #colors[0] - Background (Black)
                         ["#3760bf"],   #colors[1] - Foreground (White)
                         ["#007197"],   #colors[2] - Cyan
                         ["#2e7de9"],   #colors[3] - Blue 
                         ["#7847bd"],   #colors[4] - Purple
                         ["#9854f1"],   #colors[5] - Pink
                         ["#f52a65"],   #colors[6] - Red
                         ["#b15c00"],   #colors[7] - Orange
                         ["#8c6c3e"],   #colors[8] - Yellow
                         ["#587539"],   #colors[9] - Green
                         ["#c4c8da"],   #colors[10] - Separator/Border Normal
                         ["#007197"]],  #colors[11] - Border Focus                

"catppuccin-macchiato": [["#24273a"],   #colors[0] - Background (Black)
                         ["#cad3f5"],   #colors[1] - Foreground (White)
                         ["#91d7e3"],   #colors[2] - Cyan
                         ["#8aadf4"],   #colors[3] - Blue 
                         ["#c6a0f6"],   #colors[4] - Purple
                         ["#f5bde6"],   #colors[5] - Pink
                         ["#ed8796"],   #colors[6] - Red
                         ["#f5a97f"],   #colors[7] - Orange
                         ["#eed49f"],   #colors[8] - Yellow
                         ["#a6da95"],   #colors[9] - Green
                         ["#363a4f"],   #colors[10] - Separator/Border Normal
                         ["#b7bdf8"]],  #colors[11] - Border Focus
                         
"doom-one":             [["#282c34"],   #colors[0] - Background (Black)
                         ["#dfdfdf"],   #colors[1] - Foreground (White)
                         ["#46d9ff"],   #colors[2] - Cyan
                         ["#51afef"],   #colors[3] - Blue
                         ["#a9a1e1"],   #colors[4] - Purple
                         ["#c678dd"],   #colors[5] - Pink
                         ["#ff6c6b"],   #colors[6] - Red
                         ["#da8548"],   #colors[7] - Orange
                         ["#ecbe7b"],   #colors[8] - Yellow
                         ["#98be65"],   #colors[9] - Green
                         ["#3e4556"],   #colors[10] - Separator / Border Normal
                         ["#46d9ff"]],  #colors[11] - Border Focus

"nord":                 [["#2e3440"],   #colors[0] - Background (Black)
                         ["#eceff4"],   #colors[1] - Foreground (White)
                         ["#88c0d0"],   #colors[2] - Cyan
                         ["#81a1c1"],   #colors[3] - Blue
                         ["#b48ead"],   #colors[4] - Purple
                         ["#8fbcbb"],   #colors[5] - Pink
                         ["#bf616a"],   #colors[6] - Red
                         ["#d08770"],   #colors[7] - Orange
                         ["#ebcb8b"],   #colors[8] - Yellow
                         ["#a3be8c"],   #colors[9] - Green
                         ["#434c5e"],   #colors[10] - Separator / Border Normal
                         ["#88c0d0"]],  #colors[11] - Border Focus



}
