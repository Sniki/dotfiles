#!/usr/bin/env bash

lxsession &
nitrogen --restore &
nm-applet &
blueman-applet &
udiskie &
