# Imports
from libqtile import extension
from libqtile.config import Key, KeyChord
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

# Defaults
mod = "mod4"
terminal = guess_terminal()

keys = [
    # Lenovo ThinkPad Function Keys
    Key([], "XF86AudioMute",
        lazy.spawn("amixer sset Master toggle"),
        desc="Mute/UnMute Volume"
    ),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn("amixer sset Master 5%-"),
        desc="Lower Audio Volume"
    ),
    Key([], "XF86AudioRaiseVolume",
        lazy.spawn("amixer sset Master 5%+"),
        desc="Increase Audio Volume"
    ),
    Key([], "XF86AudioMicMute",
        lazy.spawn("amixer sset Capture toggle"),
        desc="Mute/UnMute Microphone"
    ),
    Key([], "XF86MonBrightnessDown",
        lazy.spawn("brightnessctl set 5%-"),
        desc="Lower Screen Brightness"
    ),
    Key([], "XF86MonBrightnessUp",
        lazy.spawn("brightnessctl set +5%"),
        desc="Increase Screen Brightness"
    ),
    Key([], "XF86Display",
        lazy.spawn(""),
        desc="Video Mirror"
    ),
    Key([], "XF86WLAN",
        lazy.spawn(""),
        desc="Wireless/Bluetooth Radio ON/OFF"
    ),
    Key([], "XF86Tools",
        lazy.spawn("alacritty"),
        desc="Settings"
    ),
    Key([], "XF86Search",
        lazy.spawn("rofi -terminal alacritty -show drun -icon-theme 'Papirus' -show-icons"),
        desc="Search"
    ),
    Key([], "XF86LaunchA",
        lazy.spawn("firefox"),
        desc="Launcher"
    ),
    Key([], "XF86Explorer",
        lazy.spawn("pcmanfm"),
        desc="Explorer"
    ),
    Key([], "Print",
        lazy.spawn(""),
        desc="Screenshot"
    ),
    Key([mod], "Print",
        lazy.spawn(""),
        desc="Screenshot Selection"
    ),
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", 
        lazy.layout.left(), 
        desc="Move focus to left"
    ),
    Key([mod], "l", 
        lazy.layout.right(), 
        desc="Move focus to right"
    ),
    Key([mod], "j", 
        lazy.layout.down(), 
        desc="Move focus down"
    ),
    Key([mod], "k", 
        lazy.layout.up(), 
        desc="Move focus up"
    ),
    Key([mod], "space", 
        lazy.layout.next(), 
        desc="Move window focus to other window"
    ),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", 
        lazy.layout.shuffle_left(), 
        desc="Move window to the left"
    ),
    Key([mod, "shift"], "l", 
        lazy.layout.shuffle_right(), 
        desc="Move window to the right"
    ),
    Key([mod, "shift"], "j", 
        lazy.layout.shuffle_down(), 
        desc="Move window down"
    ),
    Key([mod, "shift"], "k", 
        lazy.layout.shuffle_up(), 
        desc="Move window up"
    ),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", 
        lazy.layout.grow_left(), 
        desc="Grow window to the left"
    ),
    Key([mod, "control"], "l", 
        lazy.layout.grow_right(), 
        desc="Grow window to the right"
    ),
    Key([mod, "control"], "j", 
        lazy.layout.grow_down(), 
        desc="Grow window down"
    ),
    Key([mod, "control"], "k", 
        lazy.layout.grow_up(), 
        desc="Grow window up"
    ),
    Key([mod], "n", 
        lazy.layout.normalize(), 
        desc="Reset all window sizes"
    ),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", 
        lazy.spawn(terminal), 
        desc="Launch terminal"
    ),
    Key([mod, "shift"], "Return", lazy.run_extension(extension.DmenuRun(
        dmenu_prompt="Run:",
        dmenu_font="Sans Bold-11",
        background="#222436",
        foreground="#c8d3f5",
        selected_background="#2d3f76",
        selected_foreground="#c8d3f5",
        dmenu_height=30,  # Only supported by some dmenu forks
        desc="Launch Dmenu",
    ))),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", 
        lazy.next_layout(), 
        desc="Toggle between layouts"
    ),
    Key([mod], "w", 
        lazy.window.kill(), 
        desc="Kill focused window"
    ),
    Key([mod, "shift"], "c",
        lazy.window.kill(),
        desc='Kill active window'
    ),
    Key([mod, "shift"], "r", 
        lazy.reload_config(), 
        desc="Reload the config"
    ),
    Key([mod, "shift"], "q", 
        lazy.shutdown(), 
        desc="Shutdown Qtile"
    ),
    Key([mod], "r", 
        lazy.spawncmd(), 
        desc="Spawn a command using a prompt widget"
    ),
    # Scratchpads
    Key(["control"], "1",
        lazy.group["scratchpad"].dropdown_toggle("term"),
        desc="Launch Alacritty ScratchPad"
    ),
    Key(["control"], "2",
        lazy.group["scratchpad"].dropdown_toggle("vim"),
        desc="Launch Vim Editor ScratchPad"
    ),
    Key(["control"], "3",
        lazy.group["scratchpad"].dropdown_toggle("pcmanfm"),
        desc="Launch PCManFm ScratchPad"
    ),
    # KeyChords
    KeyChord([mod], "z", [
        Key([], "a", lazy.spawn("alacritty"),
        desc="Launch alacritty with KeyChord")
    ])
]
