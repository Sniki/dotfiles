vim9script
#  ____        _ _    _
# / ___| _ __ (_) | _(_)    Besnik Rrustemi (Sniki)
# \___ \| '_ \| | |/ / |    https://gitlab.com/sniki/
#  ___) | | | | |   <| |
# |____/|_| |_|_|_|\_\_|
#
# This is my .vimrc config file with the stuff that i use.


# Set compatibility to Vim only
set nocompatible

# Text Wrapping
set wrap

# Encoding
set encoding=utf-8

# Show line numbers
set number

# Status bar
set laststatus=2

# Syntax Highlighting
syntax on

# Lightline Status bar colorscheme
g:lightline = { colorscheme: 'one', }

# Choose between light and dark mode
set background=dark

# Call the .vimrc.plug file
if filereadable(expand("~/.vimrc.plug"))
    source ~/.vimrc.plug
endif
