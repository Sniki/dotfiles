# dotfiles

dotfiles repository to store various Linux configuration files.

Applications:
- Qtile Window Manager
- Dmenu from suckless
- Alacritty Terminal
- Fish Shell
- Starship Prompt
- Vim Text Editor
- Dunst Notification
- Zathura PDF/EPUB/Book reader
- Geany Text Editor

Supported colorschemes (WIP on some applications, they all default to Nord theme which is my favorite right now):
- Doom One
- Dracula
- Nord
- Gruvbox
- Solarized
